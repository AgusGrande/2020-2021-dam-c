package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 8
Descripcio: Algorisme que calcula la superf�cie d�un rectangle.
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int num1 = 0;
		int num2 = 0;
		
		System.out.print("Introdueix la longitud del rectangle: ");
		num1 = reader.nextInt();
		
		System.out.print("Introdueix la al�ada del rectangle: ");
		num2 = reader.nextInt();
		
		reader.close();
		
		System.out.println("La superficie del rectangle �s de "+(num1*num2)+" cent�metres quadrats");

	}

}
