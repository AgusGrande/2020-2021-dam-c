package A3Bloc1;

/*IES Sabadell.
CFGS DAM M03 UF1
Bloc 1 Exercici 3
Descripcio: Algorisme que llegeix dos n�meros i mostri com resultat qui �s el m�s gran.
Autor: David L�pez 
 */

import java.util.Scanner;

public class Exercici3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner reader = new Scanner(System.in);
		int num1 = 0;
		int num2 = 0;
		
		System.out.print("Introdueix un numero: ");
		num1 = reader.nextInt();
		
		System.out.print("Introdueix un altre numero :");
		num2 = reader.nextInt();
		
		reader.close();
		
		if (num1<num2) {
			System.out.print("el numero mes gran es "+num2);
		}
		else {
			if (num1>num2) {
				System.out.print("el numero mes gran es "+num1);
			}
		}
	}

}
